<?php

use App\App;
use Illuminate\Container\Container;



require_once "../bootsrap/autoload.php";

$container = new Container();

$app = new App($container);

$app->run();