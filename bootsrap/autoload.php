<?php

use SSD\DotEnv\DotEnv;

require_once(realpath(__DIR__ . "/../vendor/autoload.php"));
$dotEnv = new DotEnv(__DIR__ . '/../.env');

$dotEnv->load();

$dotEnv->required([
    'JOBQUEUE_REDIS_DSN'
]);