<?php


namespace App\Models;


use SSD\DotEnv\DotEnv;

class Submission
{
    /**
     * @var int
     */
    private int $project_id = 1;

    /**
     * @var string
     */
    private string $filename = "main.zip";

    /**
     * @var string
     */
    private string $codeFileName = "main.py";

    /**
     * @var string
     */
    private string $dir = "submissions";

    /**
     * @return string
     */
    public function getProjectPath(): string
    {
        $storage = DotEnv::get("STORAGE_PATH", "/home/mreza/laravel/judge/storages");
        return realpath($storage . DIRECTORY_SEPARATOR . $this->dir . DIRECTORY_SEPARATOR . $this->project_id);
    }

    /**
     * @return string
     */
    public function getCodeFilePath(): string
    {
        $storage = DotEnv::get("STORAGE_PATH", "/home/mreza/laravel/judge/storages");
        return realpath($storage . DIRECTORY_SEPARATOR . $this->dir . DIRECTORY_SEPARATOR . $this->project_id . DIRECTORY_SEPARATOR . $this->codeFileName);
    }

    public function getCodeFileName(): string
    {
        return $this->codeFileName;
    }

    /**
     * @return string
     */
    public function getProjectFile(): string
    {
        $storage = DotEnv::get("STORAGE_PATH");
        $projectPath = $this->dir . DIRECTORY_SEPARATOR . $this->project_id . DIRECTORY_SEPARATOR . $this->filename;
        return realpath($storage . DIRECTORY_SEPARATOR . $projectPath);
    }
}