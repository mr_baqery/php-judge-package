<?php


namespace App\Models;


use SSD\DotEnv\DotEnv;

class TestProject
{
    /**
     * @var string
     */
    private string $module = "my_sum";

    /**
     * @var int
     */
    private int $project_id = 1;

    /**
     * @var string
     */
    private string $filename = "test.zip";

    /**
     * @var string
     */
    private string $dir = "tests";

    /**
     * @return int
     */
    public function getProjectId(): int
    {
        return $this->project_id;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @return string
     */
    public function getProjectPath(): string
    {
        $storage = DotEnv::get("STORAGE_PATH");
        return realpath($storage . DIRECTORY_SEPARATOR . $this->dir . DIRECTORY_SEPARATOR . $this->project_id);
    }

    /**
     * @return string
     */
    public function getProjectFile(): string
    {
        $storage = DotEnv::get("STORAGE_PATH", "/home/mreza/laravel/judge/storages");
        $projectPath = $this->dir . DIRECTORY_SEPARATOR . $this->project_id . DIRECTORY_SEPARATOR . $this->filename;
        return realpath($storage . DIRECTORY_SEPARATOR . $projectPath);
    }


    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

}