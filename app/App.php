<?php


namespace App;


use App\Jobs\JobsManager;
use App\Jobs\PythonJudgeJob;
use Illuminate\Container\Container;
use JetBrains\PhpStorm\Pure;

class App
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function run()
    {
        $jobManager = new JobsManager();
        $jobManager->queueJob(PythonJudgeJob::class);
    }
}