<?php

namespace App\Judges;

use App\Models\Submission;
use App\Models\TestProject;
use App\Models\User;
use ZipArchive;

abstract class IJudge
{
    /**
     * @var string
     */
    protected string $cwd;

    /**
     * @var string
     */
    protected string $project_type;

    /**
     * @var string
     */
    protected string $version;

    /**
     * @var User
     */
    protected User $user;

    /**
     * Submission ID
     * @var Submission
     */
    protected Submission $submission;

    /**
     * Test Project id
     * @var TestProject
     */
    protected TestProject $testProject;

    public function __construct(TestProject $testProject, Submission $submission, User $user, string $project_type)
    {
        $this->user = $user;
        $this->submission = $submission;
        $this->testProject = $testProject;
        $this->project_type = $project_type;
        $this->cwd = $this->submission->getProjectPath();
    }

    /**
     * Bring test project and submission file beside together
     */
    public function getThemAllTogether()
    {
        $dest = $this->submission->getProjectPath();
        $source = $this->testProject->getProjectFile();
        copy($source, $dest . DIRECTORY_SEPARATOR . $this->testProject->getFilename());
    }

    /**
     * Unzip project
     */
    public function unZipProjects()
    {
        chdir($this->submission->getProjectPath());
        $zipFiles = glob($this->submission->getProjectPath() . DIRECTORY_SEPARATOR . "*.zip");
        $zip = new ZipArchive();

        foreach ($zipFiles as $zipFile) {
            if ($zip->open($zipFile) === true) {
                $zip->extractTo($this->submission->getProjectPath());
            }
        }


    }

    public abstract function preprocessing();

    /**
     * Execute the project
     */
    public abstract function execute();

    /**
     * Parse result of test execution
     */
    public abstract function parseResult();

    /**
     * Install requirements
     */
    public abstract function installRequirements();

}