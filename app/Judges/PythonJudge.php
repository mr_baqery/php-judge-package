<?php


namespace App\Judges;


use App\Judges\Commands\Commands;

class PythonJudge extends IJudge
{
    private string $command = Commands::PYTHON_COMMAND;
    private string $resultXML = "nosetests.xml";

    public function createInitFile()
    {
        chdir($this->cwd);
        mkdir($this->testProject->getModule());
        $content = "from {$this->testProject->getModule()}.main import *";
        $initFile = fopen("{$this->testProject->getModule()}/__init__.py", "w");
        copy($this->submission->getCodeFilePath(), $this->submission->getProjectPath() . DIRECTORY_SEPARATOR . $this->testProject->getModule() . DIRECTORY_SEPARATOR . $this->submission->getCodeFileName());
        fwrite($initFile, $content);
        fclose($initFile);
    }

    public function preprocessing()
    {
        $this->createInitFile();
    }

    public function execute()
    {
        chdir($this->cwd);

        $output = null;
        $status = null;

        exec($this->command, $output, $status);

    }

    public function parseResult(): void
    {
        chdir($this->cwd);
        $result = simplexml_load_file($this->resultXML);
        dd($result);
    }

    public function installRequirements()
    {
        // TODO: Implement installRequirements() method.
    }

}