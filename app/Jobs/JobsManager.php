<?php


namespace App\Jobs;


class JobsManager
{

    public function __construct()
    {
        \Resque::setBackend(\SSD\DotEnv\DotEnv::get("JOBQUEUE_REDIS_DSN", 'localhost:6379'));
    }

    public function queueJob(string $jobClass)
    {
        \Resque::enqueue('default', $jobClass);
    }
}