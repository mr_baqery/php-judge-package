<?php


namespace App\Jobs;


use App\Judges\PythonJudge;
use App\Models\Submission;
use App\Models\TestProject;
use App\Models\User;

class PythonJudgeJob
{
    public function perform()
    {
        $testProject = new TestProject();
        $submission = new Submission();
        $user = new User();
        $pythonJudge = new PythonJudge($testProject, $submission, $user, "python");
        $pythonJudge->getThemAllTogether();
        $pythonJudge->unZipProjects();
        $pythonJudge->preprocessing();
        $pythonJudge->execute();
        $pythonJudge->parseResult();
    }

    public function setUp()
    {
        // ... Set up environment for this job
    }

    public function tearDown()
    {
        // ... Remove environment for this job
    }
}